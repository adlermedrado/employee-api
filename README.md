# Employee API Database

This is a sample API project, written in Python using Django and Django Rest Framework.

## Dependencies

In order to run this project, you'll need to satisfy the prerequesites below:

* Python 3.6.4
* Django 2.0.2
* Django Rest Framework 3.7.7
* PostgreSQL 9.6

All dependencies are set at Pipenv file.

This project use pipenv, if don't want to use it, you'll need to create a requirements file with the dependencies to install using Pip.

## Docs

The API Docs (Swagger) is located at: http://localhost:8000/docs

## Admin

The Admin is located at: http://localhost:8000/admin

## Database Migrations

You should create a database and configure the project with its credentials, to do it you should copy the local.env file and save it as .env,
changing the value of the line: *DATABASE_URL=postgresql://dbuser:dbpass@dbhost/dbname_* using your database credentials.

To run the migration, just execute: `make migrate`

## API Usage Examples:

Create:

```
curl -H "Content-Type: application/json" -X POST -d '{"name":"xyz","email":"teste@teste.com.br","department": "Sales" }' http://localhost:8000/employee/
```

Retrieve

```
curl -H "Content-Type: application/json" http://localhost:8000/employee/
```

And so on... You can find all operations at http://localhost:8000/docs

## Run tests

Just execute `make test`
