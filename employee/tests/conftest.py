import pytest
from apps.employee_data.models import Employee


@pytest.fixture
def new_employee():
    employee = Employee()
    employee.name = 'Emeneu Cajumentício das Dores Conjugais'
    employee.email = 'emeneu@cajumenticio.com.br'
    employee.department = 'Sales'
    employee.save()
