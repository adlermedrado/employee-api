import pytest

from apps.employee_data.models import Employee

pytestmark = pytest.mark.django_db


def test_add_new_employee(new_employee):
    employee = Employee()
    employee.name = 'Maria Joaquina de Amaral Pereira Góes'
    employee.email = 'maria.joaquina@gmail.com.br'
    employee.department = 'Sales'
    employee.save()

    employees = Employee.objects.all()

    assert len(employees) == 2
    assert employees[1].name == 'Maria Joaquina de Amaral Pereira Góes'


def test_edit_employee(new_employee):
    employee = Employee.objects.filter(email='emeneu@cajumenticio.com.br')

    assert len(employee) == 1


def test_delete_employee(new_employee):
    Employee.objects.filter(email='emeneu@cajumenticio.com.br').delete()

    assert len(Employee.objects.all()) == 0
