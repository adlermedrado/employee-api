from django.contrib import admin
from apps.employee_data.models import Employee


admin.site.register(Employee)
